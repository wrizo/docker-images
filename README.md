# docker-images

Respaldo de Dockerfiles.

## Para compilar

Para compilar las imagenes se debe tener instalado docker y se puede tomar como referencia el siguiente comando:
```
docker build -t <tag-name> -f <file-name> .
```

* Compilando el archivo ubuntu_ruby_21_04, el tag-name puede variar:
```
docker build --no-cache -t  wrizo/ubuntu_ruby:3.0.2 -f ubuntu_ruby_21_04 .
```
